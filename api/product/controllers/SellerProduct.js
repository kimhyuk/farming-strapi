"use strict";

/**
 * Read the documentation () to implement custom controller functions
 */

module.exports = {
  create: async ctx => {
    let body = ctx.request.body;

    body.seller = ctx.state.user._id;
  

    const product =  await strapi.services.product.create(body);

    ctx.send(product);
  },
  delete: async ctx => {

    ctx.send(await strapi.services.product.delete({_id:ctx.params._id,seller:ctx.state.user._id}))
  },
  find: async ctx => {
    ctx.send(await strapi.services.product.find({seller:ctx.state.user._id}));
  },
  findOne: async ctx => {
    ctx.send(await strapi.services.product.findOne({_id:ctx.request.boddy,seller:ctx.state.user._id}));

  },
  update: async ctx => {
    let body = ctx.request.body;

    body.seller = ctx.state.user._id;
  

    const product =  await strapi.services.product.update({_id:ctx.params._id},body);

    ctx.send(product);
  },
  count: async ctx => {}
};
