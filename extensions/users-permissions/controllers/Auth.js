"use strict";

const _ = require("lodash");

const sanitizeUser = user => _.omit(user, ["password", "resetPasswordToken"]);

module.exports = {
  async sellerRegister(ctx) {
    let user = ctx.state.user;

    if (!user) {
      return ctx.badRequest(null, [
        { messages: [{ id: "No authorization header was found" }] }
      ]);
    }

    const roles = await strapi
      .query("role", "users-permissions")
      .find({ name: "Seller" });

    await strapi
      .query("user", "users-permissions")
      .update({ id: user.id }, { role: roles[0]._id });

    ctx.send(sanitizeUser(user));
  }
};
