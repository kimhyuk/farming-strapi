"use strict";

/**
 * An asynchronous bootstrap function that runs before
 * your application gets started.
 *
 * This gives you an opportunity to set up your data model,
 * run jobs, or perform some special logic.
 */

module.exports = async cb => {
  const roles = await strapi
    .query("role", "users-permissions")
    .find({ name: "Seller" });


  if (!roles.length) {
    const plugins = await strapi.plugins[
      "users-permissions"
    ].services.userspermissions.getPlugins("ko-KR");
    let permissions = await strapi.plugins[
      "users-permissions"
    ].services.userspermissions.getActions(plugins);

    permissions[
      "users-permissions"
    ].controllers.auth.sellerRegister.enabled = true;

    let sellerProduct = permissions.application.controllers.sellerproduct;

    sellerProduct.create.enabled = true;
    sellerProduct.update.enabled = true;
    sellerProduct.delete.enabled = true;
    sellerProduct.find.enabled = true;
    sellerProduct.findOne.enabled = true;
    sellerProduct.count.enabled = true;

    const role = await strapi.plugins[
      "users-permissions"
    ].services.userspermissions.createRole({
      name: "Seller",
      description: "sellerPermission",
      type: "seller",
      permissions: permissions
    });
  }

  cb();
};
